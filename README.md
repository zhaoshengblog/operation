# 数据分析实战训练营1期1班

#### 介绍
+ 第一次提交数据分析实战训练营作业 【阶段一模块一作业】2020-08-25 17:29
+ 第二次提交数据分析作业【阶段二模块一作业】2020-09-06 17:13
+ 第三次提交数据分析作业【阶段二模块二作业】2020-09-08 13:35
+ 第四次提交数据分析作业【阶段二模块三作业】2020-09-10 10:56
+ 第五次提交数据分析作业【阶段三模块一作业】2020-09-15 12:35
+ 第六次提交数据分析作业【阶段三模块二作业】2020-09-19 00:22
+ 第七次提交数据分析作业【阶段三模块三作业】2020-09-25 17:22
+ 第八次提交数据分析作业【阶段四模块二作业】2020-11-19 17:25
+ 第九次提交数据分析作业【阶段四模块三作业】2020-11-20 13:58
+ 第十次提交数据分析作业【阶段五模块三作业】2020-11-23 15:38
+ 第十一次提交数据分析作业【阶段六模块一作业】2020-12-07 23:38
+ 第十二次提交数据分析作业【阶段六模块二作业】2020-12-11 20:55
+ 第十三次提交数据分析作业【阶段六模块三作业】2020-12-14 22:30
+ 第十四次提交数据分析作业【阶段六模块四作业】2020-12-20 19:50
+ 第十五次提交数据分析作业【阶段六模块五作业】2021-01-31 23:50
+ 第十六次提交数据分析作业【阶段六模块六作业】2021-02-02 23:50
+ 第十七次提交数据分析作业【阶段五模块二作业】2021-02-09 22:50
+ 第十八次提交数据分析作业【阶段七模块一作业】2021-02-09 22:50
+ 第十九次提交数据分析作业【阶段七模块二作业】2021-02-11 00:50
+ 第二十次提交数据分析作业【阶段八模块一作业】2021-02-13 00:50
+ 第二十一次提交数据分析作业【阶段八模块二作业】2021-02-13 00:50
+ 第二十二次提交数据分析作业【阶段八模块三作业】2021-02-14 15:50

#### Assistants-用心做好一件事 https://www.zhaoshengblog.cn/

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)


